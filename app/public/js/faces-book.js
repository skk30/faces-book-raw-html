$(document).ready(function() {

  // ready for Javascript which will run when the html is loaded

  //const add = $('<b>').html('Hello');
  const faces = $('faces');
  const makeFace = function(name) {
    return $('<img>',{
      title:name,
      src :`img/2018/${name}.jpg`,
     class:"face"
    });
  };
  const names = ["Thiru","Thamizh","Janani","Sudeep","Ameya","John",
  "Varsha","Vishnu","Gayatri","Supriya","Santhosh","Shruti","Rishi",
  "Aishu","Mariah","Anushree","RVishnupriya","Keerthana","Akhil",
  "Mahidher","Sanjana","Karthika","Akshat","Jon","Pavithrann","Sindhu"];
  names.forEach(function(name) {
  faces.append(makeFace(name));
});

});
