'use strict';

module.exports = testRandomInt;

const randomInt = require('./../randomInt.js');
const assert = require('assert');

function testRandomInt() {
  test_randomInt_with_lower_bound_included();
  test_randomInt_with_upper_bound_excluded();
  test_all_numbers_generated();
}

function test_randomInt_with_lower_bound_included() {
  let foundLower = false;
  for(let i = 0; i < 1000 ; i++) {
    if(randomInt(1,5)=== 1) {
      foundLower = true;
    }
  }
  assert.ok(foundLower);
}

function test_randomInt_with_upper_bound_excluded() {
  let notFoundUpper = true;
  for(let i = 0; i < 1000 ; i++) {
    if(randomInt(1,5) === 5) {
      notFoundUpper = false;
    }
  }
  assert.ok(notFoundUpper);
}


function test_all_numbers_generated() {
  let lo = 1;
  let hi = 10;
  let count = [0];
  let generated = true;

  for(let i = lo; i < hi-1; i++) {
    count[i] = 0;
  }
   for(let i = 0; i < 1000 ; i++) {
     let n = randomInt(lo,hi);
     count[n-1]++;
   }
  for(let j = lo-1; j < hi-1; j++) {
    if(count[j] === 0) {
     generated = false;
    }
  }
  assert.ok(generated);
}


testRandomInt();
console.log("All tests passed");
